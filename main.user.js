// ==UserScript==
// @name         Arcanys - Sprout Auto Log
// @namespace    https://xlr8.hrhub.ph/
// @version      0.4.1
// @description  Anti Attendance PB - auto login & logout from Sprout - because it hurts you know.
// @author       Earvin Polancos
// @match        https://xlr8.hrhub.ph/EmployeeDashboard.aspx
// @match        https://xlr8.hrhub.ph/Employeedashboard.aspx
// @match        https://xlr8.hrhub.ph/Login.aspx
// @match        https://xlr8.hrhub.ph/LogIn.aspx
// @grant        none
// ==/UserScript==

const SPROUT_BASE_URL = 'https://xlr8.hrhub.ph';
const WEB_BUNDY_API_ENDPOINT = SPROUT_BASE_URL + '/Employee.aspx/SubmitBiologViaWebBundy';
const MAX_WORK_HOURS = 9.5;
const COOKIE_LOG_LIFETIME_HOURS = 18;


let ArcSprout = {};
let Cookie = {};

(function () {
    'use strict';

    let dueForLoginCheckInterval = null;
    let dueForLogoutCheckInterval = null;
    let isLogging = false;

    /**
     * ArcSprout Object
     */
    ArcSprout = {
        init: function () {

            if (!Cookie.read('userIp')) {
                $.getJSON('https://api.ipify.org?format=json', function (data) {
                    // get ip needed for web bundy log
                    Cookie.create('userIp', data.ip, 1);
                });
            }

            if (window.location.pathname.toLowerCase() == '/login.aspx') {

                Cookie.reset();

                // Tip: use chrome form autofill on sprout login form to automate login

                // attempt to auto login if form for auto-filled form
                $('body').trigger('click');

                // trigger "tab" to "use" suggested auto-filled form values by Chrome
                $('#txtUsername').trigger({
                    type: 'keypress',
                    which: 9
                });
                // replace username and password field to autofill credentials
                const $textUsernameFld = '<input name="txtUsername" type="text" id="txtUsername" value="" placeholder="Username or email">'; // supply value with email
                const $textPasswordFld = '<input name="txtPassword" type="password" id="txtPassword" value="" placeholder="Password">'; // supply value with password 
                $('#txtUsername').replaceWith($textUsernameFld);
                $('#txtPassword').replaceWith($textPasswordFld);

                setTimeout(function () {
                    // submit login form
                    $('#btnLogIn').trigger('click');
                }, 2000);


                // if still in login page and user still has no logout data
                // show alert as a reminder
                if (Cookie.read('Log In') && !Cookie.read('Log Out')) {
                    alert('No logout log yet. Login back to Sprout!');
                }

            }

            if (window.location.pathname.toLowerCase() == '/employeedashboard.aspx') {
                // attempt to display hours left
                // set delay as DOM may not yet be ready
                setTimeout(ArcSprout.showHoursLeft, 10000);

                // check if due for login
                setTimeout(ArcSprout.dueForLogin, 3000);

                if (Cookie.read('Log In') && !Cookie.read('Log Out')) {

                    if (ArcSprout.getHoursSinceLastLogin() > 9) {
                        // perform check if user is due for logout every 30 seconds
                        dueForLogoutCheckInterval = setInterval(ArcSprout.dueForLogout, 30000);
                    } else {
                        // set to reload page to avoid session timeout
                        setTimeout(function () {
                            if (!isLogging) {
                                window.location.reload();
                            }
                        }, (60000 * 2));
                    }
                }
            }
        },
        dueForLogin: function () {
            // check if user has no log in cookie set
            if (!Cookie.read('Log In')) {
                // perform login
                ArcSprout.doWebBundyLog('Log In');
            } else {
                // user 'log in' cookie has not expired yet. perform check every 30 seconds
                dueForLoginCheckInterval = setInterval(ArcSprout.dueForLogin, 30000);
            }
        },
        dueForLogout: function () {

            if (!Cookie.read('Log In') || Cookie.read('Log Out')) return;

            if (ArcSprout.getHoursLeftToLogout() < 0.00) { // probably redundant
                // perform logout
                ArcSprout.doWebBundyLog('Log Out');
            }
        },
        doWebBundyLog: function (logType) {
            if (logType == '') return;

            isLogging = true;

            $.ajax(WEB_BUNDY_API_ENDPOINT, {
                data: "{'Note': '" + logType + "', IP:'" + Cookie.read('userIp') + "'}",
                method: 'POST',
                processData: false,
                contentType: 'application/json'
            }).done(function () {
                if (logType == 'Log In') {
                    // user has now logged in. perform check if user is due for logout 30 seconds
                    dueForLogoutCheckInterval = setInterval(ArcSprout.dueForLogout, 30000); // 60000

                    // clear login interval check
                    if (!dueForLoginCheckInterval) {
                        clearInterval(dueForLoginCheckInterval);
                    }
                }

                if (logType == 'Log Out' && !dueForLogoutCheckInterval) {
                    // clear logout checking
                    clearInterval(dueForLogoutCheckInterval);
                }

                Cookie.create(logType, Date.now(), 1);

                isLogging = false;

                window.location.reload();
            });
        },
        showHoursLeft: function () {
            if (!Cookie.read('Log In')) return;

            try {
                let hoursLeftText = (ArcSprout.getHoursLeftToLogout() < 0.00 && Cookie.read('Log Out')) ? 'Auto-Logout' : ArcSprout.getHoursLeftToLogout().toFixed(2) + ' hours left';
                $($('.txt-green')[1]).html(hoursLeftText);
            } catch (e) {
            }
        },

        getHoursSinceLastLogin: function () {
            let loginDateTime = Cookie.read('Log In') || 0;
            let currentDateTime = Date.now();
            return Math.abs(loginDateTime - currentDateTime) / 36e5; // 60*60*1000
        },

        getHoursLeftToLogout: function () {
            return MAX_WORK_HOURS - ArcSprout.getHoursSinceLastLogin();
        }
    };


    /**
     * Cookie Object
     * @type {{create: create, read: read, delete: delete, reset: reset}}
     */
    Cookie = {
        create: function (name, value, days) {
            var expires;

            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            } else {
                expires = "";
            }
            document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
        },
        read: function (name) {
            var nameEQ = encodeURIComponent(name) + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ')
                    c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) === 0)
                    return decodeURIComponent(c.substring(nameEQ.length, c.length));
            }
            return null;
        },
        delete: function (name) {
            Cookie.create(name, "", -1);
        },
        reset: function () {
            if (Cookie.read('Log In')) {
                if (ArcSprout.getHoursSinceLastLogin() > COOKIE_LOG_LIFETIME_HOURS) {
                    Cookie.delete('Log In');
                    Cookie.delete('Log Out');
                }
            }
        }
    };

    $(document).ready(function () {
        ArcSprout.init();
    });
})();
